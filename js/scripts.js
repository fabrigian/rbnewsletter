(function ($, root, undefined) {

	$(function () {

		'use strict';

		$( document ).ready(function() {

			$( "#download" ).click(function() {
				$.ajax({
						type: 'POST',
						url: ajax_url,
						data: {
						
								dataType: 'json',
								action: 'downExcel',
						},
						success: function(data, textStatus, XMLHttpRequest) {
								var len = data.length;
								data = data.substring(0, len - 1);
								console.log($("#email-newsletter").val());
								console.log(data);

								var obj = jQuery.parseJSON(data);


						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
								//        alert(errorThrown);
						}
				});
			});


			$( "#newsletter" ).click(function() {
				$.ajax({
						type: 'POST',
						url: ajax_url,
						data: {
								email: $("#email-newsletter").val(),
								dataType: 'json',
								action: 'newsletterInsert',
						},
						success: function(data, textStatus, XMLHttpRequest) {
								var len = data.length;
								data = data.substring(0, len - 1);
								console.log($("#email-newsletter").val());
								console.log(data);

								var obj = jQuery.parseJSON(data);


						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
								//        alert(errorThrown);
						}
				});
		});
			});
		});

})(jQuery, this);
