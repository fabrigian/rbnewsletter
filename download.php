<?php
require_once("../../../wp-load.php");

if (current_user_can('edit_user')) {
// load library
    global $wpdb;
    $rs = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."newsletter_users");
    // generate file (constructor parameters are optional)
    $xls = new Excel_XML('UTF-8', false, 'My Test Sheet');
    $xls->addArray($rs);
    $xls->generateXML('my-test');
}

?>
