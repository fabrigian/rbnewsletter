<?php
/**
 * Plugin Name: Really Basic Newsletter
 * Description: Really Basic Newsletter with a form, a list, excel export
 * Version: 1.0.0
 * Author: Fabrizio Giannone
 *
 *
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
include( plugin_dir_path( __FILE__ ) . 'php-excel.class.php');

add_action('init', 'rbnewsletter_init');

function rbnewsletter_init() {

	wp_enqueue_script(
			'script',
			plugins_url( 'js/scripts.js', __FILE__ ),
			array( 'jquery' ), time()
	);

  wp_localize_script('gtoscripts','ajax_url', admin_url( 'admin-ajax.php' ));
  wp_enqueue_script('gtoscripts');
}

function rbnewsletter_install (){
    global $wpdb;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $table_name = $wpdb->prefix . "newsletter_users";  //get the database table prefix to create my

			 $sql = "CREATE TABLE " . $table_name . " (
					 id mediumint(9) NOT NULL AUTO_INCREMENT,
					 email varchar(120) NOT NULL,
					 PRIMARY KEY  (id)
			 ) ". $charset_collate .";";

    dbDelta( $sql );
}

add_action( 'wp_ajax_nopriv_newsletterInsert', 'newsletterInsert' );
add_action( 'wp_ajax_newsletterInsert', 'newsletterInsert' );
add_action( 'wp_ajax_nopriv_downExcel', 'downExcel' );
add_action( 'wp_ajax_downExcel', 'downExcel' );

function newsletterInsert() {
   $data=$_POST["email"];
   global $wpdb;
   $wpdb->insert($wpdb->prefix."newsletter_users",array('email'=>$data),array('%s','%s'));
  //	wpdb::insert("", $data );
   echo json_encode($data);
}

function rbnewsletter_register_options_page() {
  add_options_page('Users Newsletter', 'Users Newsletter', 'manage_options', 'rbnewsletter', 'rbnewsletter_options_page');
}
add_action('admin_menu', 'rbnewsletter_register_options_page');

function rbnewsletter_options_page() {
	global $wpdb;
	global $rs;
?>
<?php screen_icon(); ?>
<h2>Newsletter Users</h2>
<?php
$rs = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."newsletter_users");

foreach ( $rs as $r ) {
	echo $r->email."<br/>";
}
?>
<a id="download" href="<?php echo plugins_url( 'download.php', __FILE__ ); ?>"> Download </a>
<?php
}


function rbnews_shortcode() {
	ob_start();
		?>
		<div class="row">
			<div class="col-xl-9 mx-auto">
				<h2 class="mb-4">Subscribe to our newsletter</h2>
			</div>
			<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
				<form>
					<div class="form-row">
						<div class="col-12 col-md-9 mb-2 mb-md-0">
							<input id="email-newsletter" type="email" class="form-control form-control-lg" placeholder="Enter your email...">
						</div>
						<div class="col-12 col-md-3">
							<a id="newsletter" type="button" class="btn btn-block btn-lg btn-primary">Sign up!</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	<?php
		 $output = ob_get_contents();
		 ob_end_clean();
		 return $output;
}

add_shortcode('rbnews', 'rbnews_shortcode');

register_activation_hook( __FILE__, 'rbnewsletter_install' );

?>
